package com.example.cbardemoapplication.service;

import com.example.cbardemoapplication.dto.CurrencyRateDTO;
import com.example.cbardemoapplication.dto.ValTypeDto;
import com.example.cbardemoapplication.dto.ValuteDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@RequiredArgsConstructor
public class CbarRedisService  {

    private final CbarService cbarService;
    private final RedisTemplate<String, Object> redisTemplate;



    public void storeCurrenciesInRedis() {
        try {
            // Retrieve currencies data for today
            String jsonData = cbarService.retrieveCurrenciesForToday();

            // Convert JSON string to CurrencyRateDTO
            ObjectMapper objectMapper = new ObjectMapper();
            CurrencyRateDTO currencyRateDTO = objectMapper.readValue(jsonData, CurrencyRateDTO.class);

            // Store data in Redis
            for (ValTypeDto valType : currencyRateDTO.getValType()) {
                for (ValuteDto valute : valType.getValuteDtos()) {
                    String key = "currency:" + valute.getCode();
                    redisTemplate.opsForHash().put(key, "code", valute.getCode());
                    redisTemplate.opsForHash().put(key, "nominal", valute.getNominal());
                    redisTemplate.opsForHash().put(key, "name", valute.getName());
                    redisTemplate.opsForHash().put(key, "value", valute.getValue());
                }
            }

            System.out.println("Data stored in Redis successfully.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}