package com.example.cbardemoapplication.service;

import com.example.cbardemoapplication.CbarFeignClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
public class CbarService {

    private final CbarFeignClient cbarFeignClient;

    public CbarService(CbarFeignClient cbarFeignClient) {
        this.cbarFeignClient = cbarFeignClient;
    }


    public String retrieveCurrenciesForToday() throws Exception {
        String todayDate = LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));

        String xmlData = cbarFeignClient.getCurrencies(todayDate);

        return convertXMLtoJSON(xmlData);
    }

    private String convertXMLtoJSON(String xmlContent) throws Exception {
        ObjectMapper xmlMapper = new XmlMapper();
        Object xmlObject = xmlMapper.readValue(xmlContent, Object.class);
        ObjectMapper jsonMapper = new ObjectMapper();
        return jsonMapper.writeValueAsString(xmlObject);
    }
}
