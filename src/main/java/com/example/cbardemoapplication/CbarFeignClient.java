package com.example.cbardemoapplication;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "currencyClient", url = "https://www.cbar.az")
public interface CbarFeignClient {

    @GetMapping("/currencies/{date}.xml")
    String getCurrencies(@PathVariable("date") String date);

}



