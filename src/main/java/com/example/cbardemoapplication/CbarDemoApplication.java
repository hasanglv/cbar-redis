package com.example.cbardemoapplication;

import com.example.cbardemoapplication.dto.CurrencyRateDTO;
import com.example.cbardemoapplication.dto.ValTypeDto;
import com.example.cbardemoapplication.dto.ValuteDto;
import com.example.cbardemoapplication.service.CbarRedisService;
import com.example.cbardemoapplication.service.CbarService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class CbarDemoApplication implements CommandLineRunner {

	private final CbarService cbarService;
	private final CbarRedisService cbarRedisService;

    public CbarDemoApplication(CbarService cbarService, CbarRedisService cbarRedisService) {
        this.cbarService = cbarService;
        this.cbarRedisService = cbarRedisService;
    }

    public static void main(String[] args) {
		SpringApplication.run(CbarDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		cbarRedisService.storeCurrenciesInRedis();

//		try {
//			// Retrieve currencies data for today
//			String jsonData = cbarService.retrieveCurrenciesForToday();
//
//
//			// Convert JSON string to CurrencyRateDTO
//			ObjectMapper objectMapper = new ObjectMapper();
//			CurrencyRateDTO currencyRateDTO = objectMapper.readValue(jsonData, CurrencyRateDTO.class);
//
//			// Output the retrieved data
//			System.out.println("Date: " + currencyRateDTO.getDate());
//			System.out.println("Name: " + currencyRateDTO.getName());
//			System.out.println("Description: " + currencyRateDTO.getDescription());
//
//			for (ValTypeDto valType : currencyRateDTO.getValType()) {
//				System.out.println("Type: " + valType.getType());
//				for (ValuteDto valute : valType.getValuteDtos()) {
//					System.out.println("Code: " + valute.getCode());
//					System.out.println("Nominal: " + valute.getNominal());
//					System.out.println("Name: " + valute.getName());
//					System.out.println("Value: " + valute.getValue());
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}


	}
}