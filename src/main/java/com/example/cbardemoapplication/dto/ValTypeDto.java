package com.example.cbardemoapplication.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
@Data
public class ValTypeDto {
    @JsonProperty("Type")
    private String type;

    @JsonProperty("Valute")
    private List<ValuteDto> valuteDtos;

    // Getters and Setters
}

