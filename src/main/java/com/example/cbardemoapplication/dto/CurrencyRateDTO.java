package com.example.cbardemoapplication.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
@Data
public class CurrencyRateDTO {

    @JsonProperty("Date")
    private String date;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("Description")
    private String description;

    @JsonProperty("ValType")
    private List<ValTypeDto> valType;
}
