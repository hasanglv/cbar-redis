package com.example.cbardemoapplication.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ValuteDto {
    @JsonProperty("Code")
    private String code;

    @JsonProperty("Nominal")
    private String nominal;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("Value")
    private String value;
}
